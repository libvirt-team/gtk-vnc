# Czech translation for gtk-vnc.
# Copyright (C) 2010 gtk-vnc's COPYRIGHT HOLDER
# This file is distributed under the same license as the gtk-vnc package.
#
# Marek Černocký <marek@manet.cz>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: gtk-vnc master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-31 15:11+0100\n"
"PO-Revision-Date: 2010-08-26 10:36+0100\n"
"Last-Translator: Marek Černocký <marek@manet.cz>\n"
"Language-Team: Czech <gnome-cs-list@gnome.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: src/vncdisplay.c:175
msgid "Enables debug output"
msgstr "Zapne ladicí výstup"

#: src/vncdisplay.c:3502
msgid "GTK-VNC Options:"
msgstr "Volby GTK-VNC:"

#: src/vncdisplay.c:3502
msgid "Show GTK-VNC Options"
msgstr "Zobrazuje volby GTK-VNC"
