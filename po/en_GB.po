# British English translation for gtk-vnc.
# Copyright (C) 2009 gtk-vnc's COPYRIGHT HOLDER
# This file is distributed under the same license as the gtk-vnc package.
# Philip Withnall <philip@tecnocode.co.uk>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gtk-vnc master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-31 15:11+0100\n"
"PO-Revision-Date: 2009-09-08 23:54+0000\n"
"Last-Translator: Philip Withnall <philip@tecnocode.co.uk>\n"
"Language-Team: British English <en_GB@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/vncdisplay.c:175
msgid "Enables debug output"
msgstr "Enables debug output"

#: src/vncdisplay.c:3502
msgid "GTK-VNC Options:"
msgstr "GTK-VNC Options:"

#: src/vncdisplay.c:3502
msgid "Show GTK-VNC Options"
msgstr "Show GTK-VNC Options"
